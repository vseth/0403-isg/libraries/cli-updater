package updater

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"time"

	"github.com/kardianos/osext"
	"github.com/spf13/viper"
)

const (
	baseURL   = "https://tools.getsip.ethz.ch"
	schemaURL = "https://tools.vseth.ethz.ch/url-schema.json"
	viperAttr = "last_update_check"
)

// Check will call update if it has not been executed within the last day
func Check(name string) error {
	if err := viper.ReadInConfig(); err != nil {
		return fmt.Errorf("could not load viper config: %v", err)
	}
	const layout = "Mon Jan 2 15:04:05 MST 2006"
	now := time.Now()
	ts := viper.GetString(viperAttr)
	if ts != "" {
		then, err := time.Parse(layout, ts)
		// in case of error we ignore the cache (this allows some breaking changes to the way how cli-updater works)
		if err == nil && then.Add(time.Hour*24).After(now) {
			return nil
		}
	}
	viper.Set(viperAttr, now.Format(layout))
	if err := viper.WriteConfig(); err != nil {
		return fmt.Errorf("could not update cache: %v", err)
	}
	return Update(name)
}

type urlSchema struct {
	Url  string   `json:"url"`
	Vars []string `json:"vars"`
}

func Map(vs []string, f func(string, string) string, name string) []string {
	vsm := make([]string, len(vs))
	for i, v := range vs {
		vsm[i] = f(v, name)
	}
	return vsm
}

func varToVal(variable string, name string) string {
	if variable == "OS" {
		return runtime.GOOS
	}
	if variable == "ARCH" {
		return runtime.GOARCH
	}
	if variable == "name" {
		return name
	}

	return "asdf"
}

func getURL(name string) (string, error) {
	resp, errRequest := http.Get(schemaURL)
	if errRequest != nil {
		return "", errRequest
	}
	defer resp.Body.Close()

	body, errResponse := ioutil.ReadAll(resp.Body)
	if errResponse != nil {
		return "", errResponse
	}

	schema1 := urlSchema{}
	errJson := json.Unmarshal(body, &schema1)
	if errJson != nil {
		return "", errJson
	}

	url := baseURL + schema1.Url
	vars := Map(schema1.Vars, varToVal, name)
	varsInterface := make([]interface{}, len(vars))
	for i, v := range vars {
		varsInterface[i] = v
	}

	url = fmt.Sprintf(url, varsInterface...)
	return url, nil
}

// Update checks if a new version of the tool is available and ask the user if he likes to update if an update is available
func Update(name string) error {
	url, err := getURL(name)
	if err != nil {
		return fmt.Errorf("could not get URL: %v", err)
	}
	fmt.Printf("Download URL is: %s\n", url)
	// check for updates
	uptodate, err := isUpToDate(url)
	if err != nil {
		return fmt.Errorf("could not check if up-to-date (updating might resolve this issue): %v", err)
	}
	if uptodate {
		fmt.Printf("already up-to-date")
		return nil
	}
	fmt.Printf("Do you want to update %s? [Y/n]\n", name)
	reader := bufio.NewReader(os.Stdin)
	in, err := reader.ReadString('\n')
	if err != nil || len(in) == 0 {
		return nil
	}
	in = strings.ToLower(in[:len(in)-1])
	if in != "y" && in != "yes" {
		return nil
	}
	fmt.Printf("downloading from %s\n", url)
	resp, err := http.Get(url)
	if err != nil {
		return fmt.Errorf("failed to connect to the server: %v", err)
	}
	defer resp.Body.Close()
	filename, err := osext.Executable()
	if err != nil {
		return fmt.Errorf("failed to retreive the executable path: %v", err)
	}
	lastName := filename + "_old"
	fmt.Printf("rename binary to %s\n", lastName)
	err = os.Rename(filename, lastName)
	fmt.Printf("writing new file %s\n", filename)
	out, err := os.Create(filename)
	if err != nil {
		return fmt.Errorf("failed to create the file: %v", err)
	}
	defer out.Close()
	if _, err = io.Copy(out, resp.Body); err != nil {
		return fmt.Errorf("failed to write the file: "+
			"Download it manually from: %s\n%v", url, err)
	}
	if err := os.Chmod(filename, 0744); err != nil {
		return fmt.Errorf("failed to make the file executable:\n"+
			"Run `chmod +x %s` to keep using %s", filename, filepath.Base(filename))
	}
	fmt.Println("deleting " + lastName)
	err = os.Remove(lastName)
	if err != nil {
		return fmt.Errorf("failed to delete " + lastName)
	}
	fmt.Println("update successful")
	os.Exit(0)
	return nil
}

func isUpToDate(url string) (bool, error) {
	local, err := localChecksum()
	if err != nil {
		return false, fmt.Errorf("could not evaluate checksum of binary: %e", err)
	}
	remote, err := remoteChecksum(url + ".checksum")
	if err != nil {
		return false, fmt.Errorf("could not retreive remote checksum: %e", err)
	}
	return local == remote, nil
}
