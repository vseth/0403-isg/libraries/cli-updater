package updater

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/kardianos/osext"
)

func localChecksum() (string, error) {
	filename, err := osext.Executable()
	if err != nil {
		return "", err
	}
	file, err := ioutil.ReadFile(filename)
	if err != nil {
		return "", err
	}
	sum256 := sha256.Sum256(file)
	return hex.EncodeToString(sum256[:]), nil
}

func remoteChecksum(url string) (string, error) {
	resp, err := http.Get(url)
	if err != nil {
		return "", fmt.Errorf("could request checksum from server: %e", err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("could not read checksum from server: %e", err)
	}
	return strings.Split(string(body), " ")[0], nil
}
