module go.getsip.ethz.ch/cliupdater

go 1.13

require (
	github.com/kardianos/osext v0.0.0-20190222173326-2bc1f35cddc0
	github.com/spf13/viper v1.6.2
)
