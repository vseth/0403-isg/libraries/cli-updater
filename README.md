# Cli Updater

A library to check for updates and update your CLI hosted on tools.getsip.ethz.ch.

## Usage

### Download URL

Currently, cli-updater checks https://tools.vseth.ethz.ch/url-schema.json before downloading the update to find out where to download from. Change static/url-schema.json in the tool-explorer Repo to change that download path.

A json like this

{"url": "/%s/%s-%s/%s", "vars": ["name","OS","ARCH","name"]}

will turn into a download url of https://tools.vseth.ethz.ch/peoplectl/darwin-amd64/peoplectl

Valid vars are "name" (the name of the cli), "OS" and "ARCH". The number of vars and the number of %s in the url must be the same!

### Dependencies

- Viper has to be set up.

```go
home, err := os.UserHomeDir()
if err != nil {
    return fmt.Errorf("could not read user home directory: %v", err)
}
path := filepath.Join(home, ".config/vseth/<your_cli_name>")
filename := "cache"
filetype := "yaml"
viper.AddConfigPath(path)
viper.SetConfigName(filename)
viper.SetConfigType(filetype)
if err := viper.ReadInConfig(); err != nil {
    // assume a not found error and try to create the file
    if err := os.MkdirAll(path, 0744); err != nil {
        fmt.Printf("could not create path to config file: %v\n", err)
        os.Exit(1)
    }
    if err := ioutil.WriteFile(filepath.Join(path, filename+"."+filetype), []byte{}, 0644); err != nil {
        fmt.Printf("could not create config file: %v\n", err)
        os.Exit(1)
    }
}
```

#### How to use

```go
import "gitlab.ethz.ch/vseth/0403-isg/libraries/cli-updater/updater"

// Use check when your CLI starts to check for updates, checks are only done
// once a day. The error returned by Check can be ignored (a user can still update manually).
updater.Check("<your tool name>");

// to allow manual updates use
if err := updater.Update(<your tool name>); err != nil {
    fmt.Printf("failed to update: %v", err)
}
```

In case of an update the method `Check()` will never return.

## Features

- includes caching and only checks every 24h
- checks tools.getsip.ethz.ch/<your tool name>/<Go runtime OS>/<your tool name>(.checksum) for an update
